var express = require("express");

var app = express()
  , http = require('http')
  , server = http.createServer(app)
  , io = require('socket.io').listen(server);

var MongoStore = require('connect-mongo')(express);

app.use(express.cookieParser());
app.use(express.session({
  store: new MongoStore({
    url: 'mongodb://bar:bar@10.1.2.6:27017/foo'
  }),
  secret: '1234567890QWERTY'
}));

server.listen(8001);

app.get('/s/:name', function (req, res) {
	req.session.name = req.params.name;
	res.send("web1");
});

app.get('/game/display', function(req, res){
  res.sendfile(__dirname + '/game_display.html')
});
app.get('/game/play', function(req, res){
  res.sendfile(__dirname + '/game_play.html')
});

app.get('/', function (req, res) {
	res.send("web1 : " + req.session.name);
});

app.get('/map', function (req, res) {
  res.sendfile(__dirname + '/map.html');
});

io.sockets.on('connection', function (socket) {
  socket.on("vote", function(data) {
    socket.broadcast.emit('map', data);
  });
});
