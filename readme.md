This project is just me playing around with Vagrant, nodejs, socket.io, and haproxy.  
Vagrant creates a network of 4 servers: 
  load balancer using haproxy, 
  2 web servers with node, 
  and 1 database server.

supervisor -w ./app.js app.js

some ideas for service here:
https://github.com/dickeyxxx/node-sample/blob/master/ansible/app.yml

http://blog.carbonfive.com/2014/06/02/node-js-in-production/
