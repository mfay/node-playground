
class nodejs {
	package {
		"nodejs":
		ensure => installed,
		require => Exec["node4"]
	}

	exec {
		"node0":
		command => "sudo apt-get update",
		# onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
	}

	exec {
		"node1":
		command => "sudo apt-get install python-software-properties -y",
		require => Exec["node0"]
	}

	exec {
		"node2":
		command => "sudo add-apt-repository ppa:chris-lea/node.js",
		require => Exec["node1"]
	}

	exec {
		"node3":
		command => "sudo apt-get update",
		require => Exec["node2"],
		# onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
	}

	exec {
		"node4":
		command => "sudo apt-get install nodejs -y",
		require => Exec["node3"]
	}
}
