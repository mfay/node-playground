class mongodb {
	package {
		"mongodb":
		ensure => installed,
		before => File["mongodb.conf"]
	}
	file {
		"mongodb.conf":
		path => "/etc/mongodb.conf",
		owner => root,
		group => root,
		mode => 644,
		source => "puppet:///modules/mongodb/mongodb.conf",
		require => Package['mongodb']
	}
}