class haproxy {

	package {
		"haproxy":
		ensure => installed,
		before => File["/etc/haproxy/haproxy.cfg"]
	}
	file {
		"haproxy.cfg":
		path => "/etc/haproxy/haproxy.cfg",
		owner => root,
		group => root,
		mode => 644,
		source => "puppet:///modules/haproxy/haproxy.cfg"
	}
	exec {
		"haproxy start":
		command => "/usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -D",
		require => File['haproxy.cfg']
	}
}