class mysql {
	package {
		"mysql-server":
		ensure => installed,
		before => File["/etc/my.cnf"],
		require => Exec["node0"]
	}
	file {
		"/etc/my.cnf":
		owner => root,
		group => root,
		mode => 644,
		source => "puppet:///modules/mysql/my.cnf",
		require => Exec["node0"]
	}
	service {
		"mysql":
		ensure => running,
		subscribe => File["/etc/my.cnf"],
		require => Exec["node0"]
	}

	exec {
		"node0":
		command => "sudo apt-get update",
		onlyif => "/bin/bash -c 'exit $(( $(( $(date +%s) - $(stat -c %Y /var/lib/apt/lists/$( ls /var/lib/apt/lists/ -tr1|tail -1 )) )) <= 604800 ))'"
	}
	# exec {
	# 	"mysql_password":
	# 	unless => "mysqladmin -uroot -proot status",
	# 	command => "mysqladmin -uroot password root",
	# 	require => Service["mysqld"];
	# }
}