Exec {
        path => ['/usr/local/bin', '/bin', '/usr/bin', '/usr/local/sbin', '/sbin']
}

exec { "apt-update":
    command => "/usr/bin/apt-get update"
}

Exec["apt-update"] -> Package <| |>

node 'lb'  {
	include vim
	include haproxy
}

node 'web1', 'web2' {
	include vim
	include git
	include nodejs
}

node 'db' {
	include vim
	include mongodb
}
